<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List, misha.lr45.model.Declaration" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Декларації</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Декларації</h1>
    <a href="index.jsp" class="btn">Головна</a>
    <a href="DeclarationController?action=insert" class="btn">Додати нову декларацію</a>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Дата</th>
            <th>Прізвище</th>
            <th>Ім'я</th>
            <th>По батькові</th>
            <th>Громадянство</th>
            <th>Країна постійного проживання</th>
            <th>Номер паспорта</th>
            <th>Серія паспорта</th>
            <th>Країна відправлення</th>
            <th>Задекларована сума</th>
            <th>Код валюти</th>
            <th>Інформація про багаж</th>
            <th>Дії</th>
        </tr>
        <%
            List<Declaration> declarations = (List<Declaration>) request.getAttribute("declarations");
            for (Declaration declaration : declarations) {
        %>
        <tr>
            <td><%= declaration.getId() %></td>
            <td><%= declaration.getDate() %></td>
            <td><%= declaration.getLastName() %></td>
            <td><%= declaration.getFirstName() %></td>
            <td><%= declaration.getSurname() %></td>
            <td><%= declaration.getCitizenship() %></td>
            <td><%= declaration.getCountryOfPermanentResidence() %></td>
            <td><%= declaration.getPassportNumber() %></td>
            <td><%= declaration.getPassportSeries() %></td>
            <td><%= declaration.getCountryOfDeparture() %></td>
            <td><%= declaration.getDeclaredAmount() %></td>
            <td><%= declaration.getCurrencyCode() %></td>
            <td><%= declaration.getInformationOnTheAvailabilityOfLuggage() %></td>
            <td>
                <a href="DeclarationController?action=edit&decId=<%= declaration.getId() %>">Редагувати</a>
                <a href="DeclarationController?action=delete&decId=<%= declaration.getId() %>">Видалити</a>
            </td>
        </tr>
        <% } %>
    </table>
    <a href="DeclarationController?action=insert" class="btn">Додати нову декларацію</a>
</div>
</body>
</html>