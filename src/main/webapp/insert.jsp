<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Додати/редагувати декларацію</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
    <body>
    <div class="background"></div>
        <div class="content">
            <h1>Додати/редагувати декларацію</h1>
            <% if (errorMessage != null) { %>
            <div class="error-message"><%= errorMessage %></div>
            <% } %>
            <a href="index.jsp" class="btn">Головна</a>
            <a href="DeclarationController?action=showAll" class="btn">Переглянути декларації</a>
            <form action="DeclarationController" method="post">
                <input type="hidden" name="decId" value="${Declaration.id}">
                <label for="date">Дата:</label>
                <input type="text" id="date" name="date" value="${Declaration.date}">
                <label for="lastName">Прізвище:</label>
                <input type="text" id="lastName" name="lastName" value="${Declaration.lastName}">
                <label for="firstName">Ім'я:</label>
                <input type="text" id="firstName" name="firstName" value="${Declaration.firstName}">
                <label for="surname">По батькові:</label>
                <input type="text" id="surname" name="surname" value="${Declaration.surname}">
                <label for="citizenship">Громадянство:</label>
                <input type="text" id="citizenship" name="citizenship" value="${Declaration.citizenship}">
                <label for="countryOfPermanentResidence">Країна постійного проживання:</label>
                <input type="text" id="countryOfPermanentResidence" name="countryOfPermanentResidence" value="${Declaration.countryOfPermanentResidence}">
                <label for="passportNumber">Номер паспорта:</label>
                <input type="text" id="passportNumber" name="passportNumber" value="${Declaration.passportNumber}">
                <label for="passportSeries">Серія паспорта:</label>
                <input type="text" id="passportSeries" name="passportSeries" value="${Declaration.passportSeries}">
                <label for="countryOfDeparture">Країна відправлення:</label>
                <input type="text" id="countryOfDeparture" name="countryOfDeparture" value="${Declaration.countryOfDeparture}">
                <label for="declaredAmount">Задекларована сума:</label>
                <input type="text" id="declaredAmount" name="declaredAmount" value="${Declaration.declaredAmount}">
                <label for="currencyCode">Код валюти:</label>
                <input type="text" id="currencyCode" name="currencyCode" value="${Declaration.currencyCode}">
                <label for="information_on_the_availability_of_luggage">Інформація про багаж:</label>
                <input type="text" id="information_on_the_availability_of_luggage" name="information_on_the_availability_of_luggage" value="${Declaration.informationOnTheAvailabilityOfLuggage}">
                <button type="submit" class="btn">Зберегти</button>
            </form>
        </div>
    </body>
</html>