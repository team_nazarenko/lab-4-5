package misha.lr45.model;
import jakarta.persistence.*;

@Entity
@Table(name = "declaration")
public class Declaration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "date", nullable = false)
    private String date;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "citizenship", nullable = false)
    private String citizenship;

    @Column(name = "country_of_permanent_residence", nullable = false)
    private String countryOfPermanentResidence;

    @Column(name = "passport_number", nullable = false)
    private String passportNumber;

    @Column(name = "passport_series", nullable = false)
    private String passportSeries;

    @Column(name = "country_of_departure", nullable = false)
    private String countryOfDeparture;

    @Column(name = "declared_amount", nullable = false)
    private Double declaredAmount;

    @Column(name = "currency_code", nullable = false)
    private String currencyCode;

    @Column(name = "information_on_the_availability_of_luggage", nullable = false)
    private String informationOnTheAvailabilityOfLuggage;
    public Declaration(String date, String lastName, String firstName, String surname, String citizenship,
                       String countryOfPermanentResidence, String passportNumber, String passportSeries,
                       String countryOfDeparture, Double declaredAmount, String currencyCode,
                       String informationOnTheAvailabilityOfLuggage) {
        this.date = date;
        this.lastName = lastName;
        this.firstName = firstName;
        this.surname = surname;
        this.citizenship = citizenship;
        this.countryOfPermanentResidence = countryOfPermanentResidence;
        this.passportNumber = passportNumber;
        this.passportSeries = passportSeries;
        this.countryOfDeparture = countryOfDeparture;
        this.declaredAmount = declaredAmount;
        this.currencyCode = currencyCode;
        this.informationOnTheAvailabilityOfLuggage = informationOnTheAvailabilityOfLuggage;
    }

    public Declaration() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getCountryOfPermanentResidence() {
        return countryOfPermanentResidence;
    }

    public void setCountryOfPermanentResidence(String countryOfPermanentResidence) {
        this.countryOfPermanentResidence = countryOfPermanentResidence;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportSeries() {
        return passportSeries;
    }

    public void setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
    }

    public String getCountryOfDeparture() {
        return countryOfDeparture;
    }

    public void setCountryOfDeparture(String countryOfDeparture) {
        this.countryOfDeparture = countryOfDeparture;
    }

    public Double getDeclaredAmount() {
        return declaredAmount;
    }

    public void setDeclaredAmount(Double declaredAmount) {
        this.declaredAmount = declaredAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getInformationOnTheAvailabilityOfLuggage() {
        return informationOnTheAvailabilityOfLuggage;
    }

    public void setInformationOnTheAvailabilityOfLuggage(String informationOnTheAvailabilityOfLuggage) {
        this.informationOnTheAvailabilityOfLuggage = informationOnTheAvailabilityOfLuggage;
    }
    public String toString() {
        return "Declaration: " +
                "id=" + id +
                ", date='" + date + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", citizenship='" + citizenship + '\'' +
                ", countryOfPermanentResidence='" + countryOfPermanentResidence + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", passportSeries='" + passportSeries + '\'' +
                ", countryOfDeparture='" + countryOfDeparture + '\'' +
                ", declaredAmount=" + declaredAmount +
                ", currencyCode='" + currencyCode + '\'' +
                ", informationOnTheAvailabilityOfLuggage='" + informationOnTheAvailabilityOfLuggage + '\'';
    }
}
