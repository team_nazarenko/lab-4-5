package misha.lr45.service;
import misha.lr45.model.Declaration;
import misha.lr45.dao.DeclarationDAO;

import java.util.List;

public class DeclarationService {
    private static DeclarationDAO declarationDAO;

    public DeclarationService(){
        declarationDAO = new DeclarationDAO();
    }
    public void persist (Declaration entity) {
        declarationDAO.openCurrentSessionwithTransaction();
        declarationDAO.persist(entity);
        declarationDAO.closeCurrentSessionwithTransaction();
    }
    public void update (Declaration entity) {
        declarationDAO.openCurrentSessionwithTransaction();
        declarationDAO.update(entity);
        declarationDAO.closeCurrentSessionwithTransaction();
    }
    public Declaration findById(Integer id) {
        declarationDAO.openCurrentSession();
        Declaration declaration = declarationDAO.findById(id);
        declarationDAO.closeCurrnetSession();
        return declaration;
    }
    public void delete (Integer id) {
        declarationDAO.openCurrentSessionwithTransaction();
        Declaration declaration = declarationDAO.findById(id);
        declarationDAO.delete(declaration);
        declarationDAO.closeCurrentSessionwithTransaction();
    }
    public List<Declaration> findAll() {
        declarationDAO.openCurrentSession();
        List<Declaration> declarationList = declarationDAO.findAll();
        declarationDAO.closeCurrnetSession();
        return declarationList;
    }
    public void deleteAll() {
        declarationDAO.openCurrentSessionwithTransaction();
        declarationDAO.deleteAll();
        declarationDAO.closeCurrentSessionwithTransaction();
    }

    public DeclarationDAO declarationDAO () {
        return declarationDAO();
    }
}

