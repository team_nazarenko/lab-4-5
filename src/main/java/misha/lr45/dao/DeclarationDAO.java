package misha.lr45.dao;
import java.io.Serializable;
import  java.util.List;
import  org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import misha.lr45.model.Declaration;

public class DeclarationDAO implements DeclarationDAOInterface<Declaration, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;
    public DeclarationDAO(){

    }
    public Session openCurrentSession(){
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction(){
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrnetSession(){
        currentSession.close();
    }

    public void closeCurrentSessionwithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        return configuration.buildSessionFactory();
    }

    public Session getCurrentSession(){
        return currentSession;
    }
    public void setCurrentSession(Session currentSession){
        this.currentSession = currentSession;
    }

    public void setCurrentTransaction(Transaction transaction){
        this.currentTransaction = transaction;
    }

    @Override
    public void persist(Declaration entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Declaration entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Declaration findById(Integer id) {
        Declaration declaration = (Declaration) getCurrentSession().get(Declaration.class, id);
        return declaration;
    }

    @Override
    public void delete(Declaration entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Declaration> findAll() {
        List<Declaration> declaration = (List<Declaration>) getCurrentSession().createQuery("from Declaration ").list();
        return declaration;
    }

    @Override
    public void deleteAll() {
        List<Declaration> entityList = findAll();
        for(Declaration entity : entityList){
            delete(entity);
        }
    }
}
