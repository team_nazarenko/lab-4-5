package misha.lr45.dao;
import misha.lr45.model.Declaration;

import java.io.Serializable;
import java.util.List;
public interface DeclarationDAOInterface<T, Id extends Serializable> {
    public void persist(T entity);

    public void update(T entity);

    public T findById(Id id);
    public void delete (T entity);
    public List<T> findAll();
    public void deleteAll();
}
