package misha.lr45.util;

import misha.lr45.model.Declaration;
import misha.lr45.model.DeclarationModel;

public class сonverter {
    public static void Declaration_ModelToEntity(Declaration declaration, DeclarationModel model ) {
        declaration.setDate(model.getDate());
        declaration.setLastName(model.getLastName());
        declaration.setFirstName(model.getFirstName());
        declaration.setSurname(model.getSurname());
        declaration.setCitizenship(model.getCitizenship());
        declaration.setCountryOfPermanentResidence(model.getCountryOfPermanentResidence());
        declaration.setPassportNumber(model.getPassportNumber());
        declaration.setPassportSeries(model.getPassportSeries());
        declaration.setCountryOfDeparture(model.getCountryOfDeparture());
        declaration.setDeclaredAmount(model.getDeclaredAmount());
        declaration.setCurrencyCode(model.getCurrencyCode());
        declaration.setInformationOnTheAvailabilityOfLuggage(model.getInformationOnTheAvailabilityOfLuggage());
    }
}