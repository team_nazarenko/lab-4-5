package misha.lr45;

import misha.lr45.model.Declaration;
import misha.lr45.service.DeclarationService;

import java.util.List;

public class App {

    public static void main(String[] args) {
        DeclarationService declarationService = new DeclarationService();

        List<Declaration> declarationList = declarationService.findAll();
        // Виведення всіх записів з бази даних
        System.out.println("Усі декларації:");
        for(var dec : declarationList){
            System.out.println(dec);
        }
        var declaration = declarationService.findById(1);
        System.out.println(declaration);
    }
}