package misha.lr45.controller;

import misha.lr45.util.сonverter;
import misha.lr45.model.Declaration;
import misha.lr45.model.DeclarationModel;
import misha.lr45.service.DeclarationService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.util.StringUtils;

@WebServlet(urlPatterns = "/DeclarationController")
public class DeclarationController extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/insert.jsp";
    private static String LIST_USER = "/showAll.jsp";
    private DeclarationService declarationService;

    public DeclarationController() {
        super();
        declarationService = new DeclarationService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        List<Declaration> declarationList = new ArrayList<>();

        if (action.equalsIgnoreCase("delete")) {
            int decId = Integer.parseInt(request.getParameter("decId"));
            declarationService.delete(decId);
            forward = LIST_USER;
            declarationList = declarationService.findAll();
            request.setAttribute("declarations", declarationList);
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int decId = Integer.parseInt(request.getParameter("decId"));
            Declaration declaration = declarationService.findById(decId);
            request.setAttribute("Declaration", declaration);
        } else if (action.equalsIgnoreCase("showAll")) {
            forward = LIST_USER;
            declarationList = declarationService.findAll();
            request.setAttribute("declarations", declarationList);
        } else {
            forward = INSERT_OR_EDIT;
            request.setAttribute("declarations", declarationList);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DeclarationModel declarationModel = new DeclarationModel();
        declarationModel.setDate(request.getParameter("date"));
        declarationModel.setLastName(request.getParameter("lastName"));
        declarationModel.setFirstName(request.getParameter("firstName"));
        declarationModel.setSurname(request.getParameter("surname"));
        declarationModel.setCitizenship(request.getParameter("citizenship"));
        declarationModel.setCountryOfPermanentResidence(request.getParameter("countryOfPermanentResidence"));
        declarationModel.setPassportNumber(request.getParameter("passportNumber"));
        declarationModel.setPassportSeries(request.getParameter("passportSeries"));
        declarationModel.setCountryOfDeparture(request.getParameter("countryOfDeparture"));
        declarationModel.setDeclaredAmount(Double.parseDouble(request.getParameter("declaredAmount")));
        declarationModel.setCurrencyCode(request.getParameter("currencyCode"));
        declarationModel.setInformationOnTheAvailabilityOfLuggage(request.getParameter("information_on_the_availability_of_luggage"));
        // Перевірка обов'язкових полів
        if (declarationModel.getDate() == null || declarationModel.getDate().isEmpty()
                || declarationModel.getLastName() == null || declarationModel.getLastName().isEmpty()
                || declarationModel.getFirstName() == null || declarationModel.getFirstName().isEmpty()
                || declarationModel.getSurname() == null || declarationModel.getSurname().isEmpty()
                || declarationModel.getCitizenship() == null || declarationModel.getCitizenship().isEmpty()
                || declarationModel.getCountryOfPermanentResidence() == null || declarationModel.getCountryOfPermanentResidence().isEmpty()
                || declarationModel.getPassportNumber() == null || declarationModel.getPassportNumber().isEmpty()
                || declarationModel.getPassportSeries() == null || declarationModel.getPassportSeries().isEmpty()
                || declarationModel.getCountryOfDeparture() == null || declarationModel.getCountryOfDeparture().isEmpty()
                || declarationModel.getDeclaredAmount() == null || Double.isNaN(declarationModel.getDeclaredAmount())
                || declarationModel.getCurrencyCode() == null || declarationModel.getCurrencyCode().isEmpty()
                || declarationModel.getInformationOnTheAvailabilityOfLuggage() == null || declarationModel.getInformationOnTheAvailabilityOfLuggage().isEmpty()) {

            request.setAttribute("errorMessage", "Please fill in all required fields.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Перевірка формату дати
        if (StringUtils.isEmpty(declarationModel.getDate())) {
            request.setAttribute("errorMessage", "Date is required.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateFormat.parse(declarationModel.getDate());
        } catch (ParseException e) {
            request.setAttribute("errorMessage", "Invalid date format. Please use yyyy-MM-dd.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Перевірка формату суми декларації
        if (declarationModel.getDeclaredAmount() <= 0) {
            request.setAttribute("errorMessage", "Declared amount must be a positive number.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Додаткові перевірки
        // Перевірка на валідність номера паспорта
        if (StringUtils.isEmpty(declarationModel.getPassportNumber()) || !declarationModel.getPassportNumber().matches("^[A-Z0-9]{6,12}$")) {
            request.setAttribute("errorMessage", "Invalid passport number. It should contain 6 to 12 alphanumeric characters.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Перевірка на валідність серії паспорта
        if (StringUtils.isEmpty(declarationModel.getPassportSeries()) || !declarationModel.getPassportSeries().matches("^[A-Z0-9]{2,4}$")) {
            request.setAttribute("errorMessage", "Invalid passport series. It should contain 2 to 4 alphanumeric characters.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Перевірка на валідність коду валюти
        if (StringUtils.isEmpty(declarationModel.getCurrencyCode()) || !declarationModel.getCurrencyCode().matches("^[A-Z]{3}$")) {
            request.setAttribute("errorMessage", "Invalid currency code. It should contain 3 uppercase letters.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Якщо валідація пройшла успішно, продовжуємо обробку
        String decId = request.getParameter("decId");
        if (decId == null || decId.isEmpty()) {
            Declaration declarationEntity = new Declaration();
            сonverter.Declaration_ModelToEntity(declarationEntity, declarationModel);
            declarationService.persist(declarationEntity); // Сохраняем новую сущность в базе данных
        } else {
            declarationModel.setId(Integer.parseInt(decId));
            Declaration declarationEntity = declarationService.findById(declarationModel.getId()); // Получаем существующую сущность из базы данных
            if (declarationEntity == null) {
                // Обработка случая, когда сущность не найдена в базе данных
                request.setAttribute("errorMessage", "Declaration not found.");
                RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
                view.forward(request, response);
                return;
            }
            сonverter.Declaration_ModelToEntity(declarationEntity, declarationModel);
            declarationService.update(declarationEntity); // Обновляем существующую сущность в базе данных
        }
        List<Declaration> declarationList = declarationService.findAll();
        request.setAttribute("declarations", declarationList);

        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        view.forward(request, response);
    }
}